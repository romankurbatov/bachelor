X <- readRDS(file = "data/concentric_data.rds")
result <- readRDS(file = "clustered/concentric_svc.rds")

colors <- character(length(result$labels))
colors[result$labels == 1] <- "red"
colors[result$labels == 2] <- "skyblue"

cairo_pdf(filename = "figures/concentric_svc.pdf", width = 5, height = 5)
plot(X, type = "p", col = colors, pch = 16, xlab = "", ylab = "")
#points(result$unbounded.support.vectors, col = "black", pch = 1)
dev.off()
