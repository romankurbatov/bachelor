library(fpc)
library(ClusterR)
library(xtable)

X <- readRDS(file = "data/overlapping_data.rds")
y <- readRDS(file = "data/overlapping_labels.rds")

t <-data.frame(
  integer(0),
  numeric(0),
  numeric(0)
)
names(t) <- c("Кол-во кластеров", "Кал.-Хар.", "Рэнда")

k <- 2:5
for (i in seq(k)) {
  result <- kmeans(X, k[i], nstart = 50)
  ch <- calinhara(X, fitted(result, "classes"))
  adj.rand <- external_validation(y, fitted(result, "classes"))
  t[i,] <- list(k[i], ch, adj.rand)
}

xt <- xtable(t, caption = paste(
  "Кластеризация варианта модельных данных с перекрывающимися кластерами",
  "при помощи k-means для различного числа кластеров"),
  label = "tab:overlapping_kmeans", align = "c|c||c|c|",
  display = c("d","d","f","g"))
print(xt, file = "tables/overlapping_kmeans.tex", include.rownames = FALSE,
      table.placement = "!h")
