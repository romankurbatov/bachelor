source("svc.R")

X <- readRDS(file = "data/overlapping_data.rds")

p <- 0.7
q <- 0.4

result <- svc(X, p, q)

saveRDS(result, file = "clustered/overlapping_svc.rds")
