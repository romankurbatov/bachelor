library(stats)
library(datasets)
data("iris")

X <- prcomp(iris[,1:4])$x[,1:2]

true.labels <- integer(nrow(iris))
true.labels[iris[,5]=="setosa"] <- 1
true.labels[iris[,5]=="versicolor"] <- 2
true.labels[iris[,5]=="virginica"] <- 3

kmeans.labels <- readRDS("clustered/iris_kmeans.rds")

labels <- integer(length(kmeans.labels))
labels[kmeans.labels == 1] <- 1
labels[kmeans.labels == 3] <- 2
labels[kmeans.labels == 2] <- 3

colors <- character(length(labels))
colors[labels == 1] <- "red"
colors[labels == 2] <- "skyblue"
colors[labels == 3] <- "green"
colors[labels != true.labels] <- "black"

cairo_pdf(filename = "figures/iris_kmeans.pdf", width = 5, height = 5)
plot(X, type = "p", col = colors, xlab = "", ylab = "", pch = 20, cex = 0.8)
dev.off()
