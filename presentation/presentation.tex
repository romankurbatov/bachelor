\documentclass[10pt,pdf,hyperref={unicode}]{beamer}

\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[russian]{babel}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{physics}
\usepackage[onecol]{hackthefootline}
\usepackage{caption}

%\usepackage{beamerthemeAntibes}
\usetheme{Antibes}
\useoutertheme{infolines}

\htfconfig{title=none,date=short}

\newcommand{\term}[1]{{\itshape #1}}
\newcommand{\clusters}{\mathcal{C}}
\newcommand{\pars}[1]{\left(#1\right)} % parentheses
\newcommand{\brks}[1]{\left[#1\right]} % brackets
\newcommand{\brcs}[1]{\left\{#1\right\}} % braces
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\Sum}{\sum\limits}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\T}{\mathrm{T}}
\newcommand{\Lagrangian}{\mathcal{L}}

\setbeamerfont{footline}{size=\fontsize{8}{10}\selectfont}
\setbeamertemplate{caption}[numbered]

\title{Исследование результатов применения SV-подхода к решению задачи кластеризации}
\author{Р.\,Ю.\,Курбатов}
\date{23.06.2020}

\begin{document}
{
\makeatletter
\setbeamertemplate{headline}[default]
\def\beamer@entrycode{\vspace*{-\headheight}}
\makeatother	
\begin{frame}[plain]
	\begin{center}
		Санкт-Петербургский политехнический университет Петра Великого\\
		Институт прикладной математики и механики \\
		Высшая школа прикладной математики и вычислительной физики
	\vfill
	\begin{Large}
		Исследование результатов применения SV-подхода \\
		\vspace{0.2cm}
		к решению задачи кластеризации
	\end{Large}
	\vfill
	Выполнил студент гр. 3630102/60301 Р.\,Ю.\,Курбатов\\
	\vspace{0.2cm}
	Научный руководитель: доц., к.ф.-м.н. Л.\,В.\,Павлова
	\vfill
	Санкт-Петербург \\
	2020
	\end{center}
\end{frame}
}

\section{Введение}

\begin{frame}{Цели}
	Цели работы:
	\begin{itemize}
		\item Изучение SV-подхода к решению задачи кластеризации
		\item Реализация SV-алгоритма на языке R
		\item Тестирование алгоритма на модельных данных и данных из репозитория
		\item Сравнение SV-алгоритма с алгоритмом k-means
	\end{itemize}
\end{frame}
	
\begin{frame}{Постановка задачи кластеризации}
Исходные данные: $X = \brcs{\vec{x}_i}_{i=1}^N \subset \Real^d$ \\
Задача --- построить разбиение $\clusters = \brcs{C_1, \ldots, C_K}$:
\begin{equation}
\bigcup\limits_{i=1}^K C_i = X; \quad
C_i \cap C_j = \varnothing \quad \forall i \neq j
\end{equation}
Требуется найти такое разбиение, чтобы:
\begin{itemize}
	\item Векторы внутри одного кластера были как можно больше \textbf{похожи друг на друга}
	\item Векторы, принадлежащие различным кластерам, как можно больше \textbf{отличались друг от друга}
\end{itemize}
\end{frame}

\begin{frame}{Меры различия}
	\begin{itemize}
		\item Метрика Минковского:
		\begin{equation}
		\rho(\vec{x}_i,\vec{x}_j)=\pars{\Sum_{k=1}^d \abs{x_i^{(k)} - x_j^{(k)}}^n}^{1/n}
		\end{equation}
		\begin{itemize}
			\item $n = 1$ манхэттенская метрика
			\item $n = 2$ евклидова метрика
			\item $n \to \infty$  метрика Чебышёва
		\end{itemize}
		
		\item Расстояние Махаланобиса:
		\begin{equation}
		\rho(\vec{x},C)=\sqrt{\pars{\vec{x}-\bar{C}}^\T S^{-1} \pars{\vec{x}-\bar{C}}}
		\end{equation}
		$\bar{C}$ --- центр кластера $C$, $S$ --- его ковариационная матрица.
	\end{itemize}
\end{frame}

\begin{frame}{Оценка качества кластеризации: внутренние критерии}
	\begin{itemize}
		\item Критерий Дэвиса-Болдина:
		\begin{equation}
		DB(\clusters)=\frac{1}{K}\Sum_{C_k\in\clusters}\max\limits_{C_l\in\clusters\setminus C_k}\brcs
		{\frac{S(C_k)+S(C_l)}{\rho_e\pars{\bar{C}_k,\bar{C}_l}}},
		\end{equation}
		где $S(C_k)=\frac{1}{\abs{C_k}}
		\Sum_{x_i\in C_k}\rho_e\pars{x_i,\bar{C}_k}$,
		$\rho_e$ --- евклидова метрика
		
		\item Критерий Калинского-Харабаша:
		\begin{equation}
		CH(\clusters) = \frac{N-K}{K-1}\cdot\frac
		{\Sum_{C_k\in\clusters} \abs{C_k}\rho\pars{\overline{C_k},\overline{X}}}
		{\Sum_{C_k\in\clusters}\Sum_{x_i\in C_k}\rho\pars{x_i,\overline{C_k}}},
		\end{equation}
		где $\abs{C_k}$ --- количество элементов в кластере $C_k$,
		$\overline{X} = \frac{1}{N} \Sum_{x_i \in X} x_i$
		\item Другие критерии (Данна, Силуэт и т.д.)
	\end{itemize}
\end{frame}

\begin{frame}{Оценка качества кластеризации: внешние критерии}
	$\clusters = \brcs{C_1, \ldots, C_K}$, $\mathcal{D} = \brcs{D_1, \ldots, D_L}$ --- два разбиения. 
	
	\begin{itemize}
		\item $n_{ij}$ --- количество образцов, попавших одновременно в кластеры $C_i$ и $D_j$
		\item $a_i$ и $b_j$ --- количество образцов, попавших в $C_i$ и $D_j$, соответственно
	\end{itemize}
	
	Улучшенный критерий Рэнда:
	\begin{equation}
	ARI(\clusters) = \frac
	{\sum\limits_{i,j}C_{n_{ij}}^2 -
		\brks{\sum\limits_{i}C_{a_i}^2 \sum\limits_{j}C_{b_j}^2} / C_n^2}
	{\brks{\sum\limits_{i}C_{a_i}^2 + \sum\limits_{j}C_{b_j}^2}/2 -
		\brks{\sum\limits_{i}C_{a_i}^2 \sum\limits_{j}C_{b_j}^2} / C_n^2},
	\end{equation}
\end{frame}

\section{SV-алгоритм кластеризации}
\begin{frame}{Основная идея SV-алгоритма}
\begin{equation}
X = \brcs{\vec{x}_i}_{i=1}^N \subset \Real^d
\end{equation}
\begin{enumerate}
		\item $\Real^d$ --- пространство данных, $Y$ --- пространство признаков
		
		Используем отображение $\Phi \colon \Real^d \to Y, \quad \vec{x} \xmapsto{\Phi} \Phi(\vec{x})$
	
	\item В $Y$ ищем \textbf{сферу наименьшего радиуса}, охватывающую все $\Phi(\vec{x})$ \\
	 Добавляем ослабляющие переменные $\xi_j$:
	
	\begin{equation} \left\{ \begin{gathered}
	R \to \min \\
	\xi_j \geqslant 0 \\
	\norm{\Phi(\vec{x}_j) - \vec{a}}^2 \leqslant R^2 + \xi_j \quad \forall j
	\end{gathered} \right. \end{equation}
	
	\item Прообраз этой сферы в $\Real^d$ распадается на контуры, которые интерпретируют как границы кластеров
\end{enumerate}
\end{frame}

\begin{frame}{Поиск сферы минимального радиуса}
	Двойственная задача в формулировке Вольфа:
	\begin{equation} \left\{ \begin{gathered}
	W = \Sum_j K(\vec{x}_j,\vec{x}_j) \beta_j - \Sum_{i,j} \beta_i \beta_j K(\vec{x}_i, \vec{x}_j) \to \max_{\beta} \\
	0 \leqslant \beta_j \leqslant C, \quad j = 1, \ldots, N \\
	\Sum_{j=1}^N \beta_j = 1
	\end{gathered} \right. \end{equation}
	\begin{equation}
	K(\vec{x}_i,\vec{x}_j)=\Phi(\vec{x}_i)\cdot\Phi(\vec{x}_j); \quad
	K(\vec{x}_i,\vec{x}_j)=e^{-q\norm{\vec{x}_i-\vec{x}_j}^2}
	\end{equation}
	\begin{itemize}
		\item Опорные векторы (SVs): $0 < \beta_j < C$, $\Phi(\vec{x}_j)$ на поверхности сферы
		\item Связанные опорные векторы (BSVs): $\beta_j = C$, $\Phi(\vec{x}_j)$ вне сферы
		\item Остальные точки: $\beta_j=0$, $\Phi(\vec{x}_j)$ внутри сферы или на поверхности
	\end{itemize}
	\begin{equation}
	R^2(\vec{x}) = K(\vec{x},\vec{x}) - 2\Sum_j \beta_j K(\vec{x}_j, \vec{x}) +\Sum_{i,j} \beta_i \beta_j K(\vec{x}_i,\vec{x}_j)
	\end{equation}
\end{frame}

\begin{frame}{Присвоение меток. Гиперпараметры}
	\begin{itemize}
		\item Построение графа: 
		\begin{itemize}
			\item Вершины --- исходные векторы, кроме BSVs
			\item Матрица смежности:
			\begin{equation}
			A_{ij} = \begin{cases}
			1, &\text{если $\forall\vec{y}$ на отрезке, соединяющем $\vec{x}_i$ и $\vec{x}_j$, $R(\vec{y}) \leqslant R$} \\
			0, &\text{иначе}
			\end{cases}
			\end{equation}
		\end{itemize}
	
	Кластеры --- компоненты связности \\
	BSVs --- относим к ближайшему кластеру
	
	\item Гиперпараметры: $q$, $C$
	\begin{equation}
	C \to p = \frac{1}{NC}
	\end{equation}
	
	Начальные значения гиперпараметров:
	\begin{equation}
	p_0 = \frac{1}{N}; \quad q_0 = \frac{1}{\max\limits_{i,j} \norm{\vec{x}_i-\vec{x}_j}^2}
	\end{equation}
	\end{itemize}
\end{frame}

\section{Эксперименты и результаты}
\subsection{Модельные данные}
\begin{frame}{Модельные данные с перекрывающимися кластерами}
	\begin{minipage}{0.7\textwidth}
	\begin{figure}[!h]
		\centering
		\includegraphics[scale=0.6]{../figures/overlapping_svc.pdf}
		\caption{Разбиение модельных данных с перекрывающимися кластерами SV-алгоритмом}
	\end{figure}
	\end{minipage}
	\begin{minipage}{0.25\textwidth}
		\begin{center}
			\begin{align*}
				p &= 0.7 \\
				q &= 0.4
			\end{align*}
			\vfill
			SVs: 8 \\
			BSVs: 207
		\end{center}
	\end{minipage}
\end{frame}

\begin{frame}{Модельные данные с концентрическими кластерами}	
	\begin{figure}[!h]
		\begin{minipage}[pos=c,contentpos=c]{0.45\textwidth}
			\centering
			\includegraphics[scale=0.5]{../figures/concentric_svc.pdf}
			\captionof{figure}{Разбиение данных с помощью SVC}
		\end{minipage} \hfill
		\begin{minipage}[pos=c,contentpos=c]{0.45\textwidth}
			\centering
			\includegraphics[scale=0.5]{../figures/concentric_kmeans.pdf}
			\captionof{figure}{Разбиение данных с помощью k-means}
		\end{minipage}
	\end{figure}
\end{frame}

\subsection{Данные Iris}

\begin{frame}{Данные Iris: Сравнение результатов кластеризации}
		\begin{figure}[!h]
			\begin{minipage}{0.45\textwidth}
				\centering
				\includegraphics[scale=0.5]{../figures/iris_svc.pdf}
				\captionof{figure}{Разбиение данных Iris при помощи SVC}
			\end{minipage}
			\begin{minipage}{0.45\textwidth}
				\centering
				\includegraphics[scale=0.5]{../figures/iris_kmeans.pdf}
				\captionof{figure}{Разбиение данных Iris при помощи k-means}
			\end{minipage}
		\end{figure}
\end{frame}

\begin{frame}{Данные Iris: Сравнение результатов кластеризации}
	\begin{minipage}{0.4\textwidth}
		Количество кластеров в \\ эталонном разбиении --- 3
		\begin{table}
			\centering
			\begin{tabular}{|c|c|}
				\hline
				CH & DB \\
				\hline
				575.81 & 0.78 \\
				\hline
			\end{tabular}
			\caption{Значения критериев качества для эталонного разбиения}
		\end{table}
		\begin{table}
			\centering
			\begin{tabular}{|c|c||c|c|c||c|}
				\hline
				$p$ & $q$ & K & CH & DB & AR \\
				\hline
				0.01 & 0.4 & 2 & 557.46 & \bfseries{0.41} & 0.57 \\
				0.7 & 3.9 & 3 & \bfseries{613.24} & 0.74 & \bfseries{0.92} \\
				0.2 & 7 & 4 & 252.78 & 0.48 & 0.56 \\
				\hline
			\end{tabular}
			\caption{Значения критериев качества (SVC)}
		\end{table}
	\end{minipage}
	\hfill
	\begin{minipage}{0.4\textwidth}
		\begin{table}
			\centering
			\begin{tabular}{|c||c|c||c|}
				\hline
				K & CH & DB & AR \\
				\hline
				2 & 570.84 & \bfseries{0.45} & 0.54 \\
				3 & 693.71 & 0.65 & \bfseries{0.72} \\
				4 & \bfseries{719.46} & 0.69 & 0.61 \\
				5 & 685.03 & 0.70 & 0.61 \\
				6 & 705.50 & 0.82 & 0.46 \\
				7 & 696.77 & 0.85 & 0.48 \\
				\hline
			\end{tabular}
			\caption{Значения критериев качества (k-means)}
		\end{table}
	\end{minipage}
\end{frame}

\subsection{Данные Glass}
\begin{frame}{Данные Glass: Сравнение результатов кластеризации}	
	\begin{minipage}{0.4\textwidth}
		Количество кластеров в \\ эталонном разбиении --- 6
		\begin{table}
			\centering
			\begin{tabular}{|c|c|}
				\hline
				CH & DB \\
				\hline
				19.75 & 4.25 \\
				\hline
			\end{tabular}
			\caption{Значение критериев для эталонного разбиения}
		\end{table}
		\begin{table}
			\centering
			\begin{tabular}{|c|c||c|c|c||c|}
				\hline
				$p$ & $q$ & K & CH & DB & AR \\
				\hline
				0.1 & 0.2 & 2 & 17.58 & \textbf{0.35} & 0.01 \\
				0.3 & 1.1 & 3 & \textbf{125.84} & 0.87 & 0.19 \\
				0.45 & 4 & 7 & 69.45 & 1.26 & 0.24 \\
				0.4 & 5 & 10 & 59.68 & 1.50 & \textbf{0.25} \\
				\hline
			\end{tabular}
			\caption{Значения критериев качества (SVC)}
		\end{table}
	\end{minipage}
	\hfill
	\begin{minipage}{0.4\textwidth}
		\begin{table}
			\centering
			\begin{tabular}{|c||c|c||c|}
				\hline
				K & CH & DB & AR \\
				\hline
				2 & \textbf{135.68} & 1.24 & 0.19 \\
				3 & 135.51 & 1.11 & 0.23 \\
				4 & 122.75 & 0.98 & 0.25 \\
				5 & 123.72 & 1.03 & 0.25 \\
				6 & 125.44 & 1.05 & 0.26 \\
				7 & 124.95 & \textbf{0.95} & \textbf{0.27} \\
				8 & 119.70 & 0.99 & 0.27 \\
				9 & 115.64 & 1.08 & 0.22 \\
				10 & 113.57 & 0.99 & 0.22 \\
				\hline
			\end{tabular}
			\caption{Значения критериев качества (k-means)}
		\end{table}
	\end{minipage}
\end{frame}

\section{Заключение}
\begin{frame}{Заключение}
	\begin{itemize}
		\item Изучен и реализован на языке R SV-алгоритм кластеризации
		\item Проведено тестирование алгоритма на модельных данных
		\item Получены результаты SV-кластеризации данных из репозитория
		\item Проведено сравнение качества SV-кластеризации и k-means-кластеризации
	\end{itemize}
	
	Выводы:
	\begin{itemize}
		\item Преимущество SV-алгоритма:
		\begin{itemize}
			\item Возможность определять кластеры сложной структуры
			\item Возможность разделять сильно перекрывающиеся кластеры
		\end{itemize}
		\item Недостатки SV-алгоритма:
		\begin{itemize}
			\item Сложность подбора гиперпараметров
			\item Высокая вычислительная сложность
		\end{itemize}
		\item Сравнение работы алгоритмов необходимо проводить, отталкиваясь от конкретной задачи
	\end{itemize}
\end{frame}

\end{document}
