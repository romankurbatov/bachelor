X <- readRDS(file = "data/concentric_data.rds")
labels <- readRDS(file = "clustered/concentric_kmeans.rds")

colors <- character(length(labels))
colors[labels == 1] <- "red"
colors[labels == 2] <- "skyblue"

cairo_pdf(filename = "figures/concentric_kmeans.pdf", width = 5, height = 5)
plot(X, type = "p", col = colors, pch = 16, xlab = "", ylab = "")
#points(result$unbounded.support.vectors, col = "black", pch = 1)
dev.off()
