DATASET_DIR = data
FIGURES_DIR = figures
TABLES_DIR = tables
CLUSTERED_DIR = clustered
THESIS_DIR = thesis

BASIC_DATASET =				\
	$(DATASET_DIR)/basic_data.rds 		\
	$(DATASET_DIR)/basic_labels.rds

BASIC_TRUE_FIGURE =			\
	$(FIGURES_DIR)/basic_true.pdf

BASIC_SVC_TABLE =			\
	$(TABLES_DIR)/basic_svc.tex

BASIC_SVC_CLUSTERED =			\
	$(CLUSTERED_DIR)/basic_svc.rds

BASIC_SVC_FIGURE =			\
	$(FIGURES_DIR)/basic_svc.pdf

OVERLAPPING_DATASET =			\
	$(DATASET_DIR)/overlapping_data.rds	\
	$(DATASET_DIR)/overlapping_labels.rds

OVERLAPPING_TRUE_FIGURE =		\
	$(FIGURES_DIR)/overlapping_true.pdf

OVERLAPPING_KMEANS_TABLE =		\
	$(TABLES_DIR)/overlapping_kmeans.tex

OVERLAPPING_SVC_TABLE =		\
	$(TABLES_DIR)/overlapping_svc.tex

OVERLAPPING_SVC_CLUSTERED =			\
	$(CLUSTERED_DIR)/overlapping_svc.rds

OVERLAPPING_KMEANS_CLUSTERED =			\
	$(CLUSTERED_DIR)/overlapping_kmeans.rds

OVERLAPPING_SVC_FIGURE =			\
	$(FIGURES_DIR)/overlapping_svc.pdf

OVERLAPPING_KMEANS_FIGURE =			\
	$(FIGURES_DIR)/overlapping_kmeans.pdf

CONCENTRIC_DATASET =			\
	$(DATASET_DIR)/concentric_data.rds	\
	$(DATASET_DIR)/concentric_labels.rds

CONCENTRIC_SVC_CLUSTERED =			\
	$(CLUSTERED_DIR)/concentric_svc.rds

CONCENTRIC_KMEANS_CLUSTERED =			\
	$(CLUSTERED_DIR)/concentric_kmeans.rds

CONCENTRIC_SVC_FIGURE =			\
	$(FIGURES_DIR)/concentric_svc.pdf

CONCENTRIC_KMEANS_FIGURE =			\
	$(FIGURES_DIR)/concentric_kmeans.pdf

IRIS_SVC_CLUSTERED =			\
	$(CLUSTERED_DIR)/iris_svc.rds

IRIS_SVC_FIGURE =			\
	$(FIGURES_DIR)/iris_svc.pdf

IRIS_TRUE_FIGURE =			\
	$(FIGURES_DIR)/iris_true.pdf

IRIS_KMEANS_CLUSTERED =			\
	$(CLUSTERED_DIR)/iris_kmeans.rds

IRIS_KMEANS_FIGURE =			\
	$(FIGURES_DIR)/iris_kmeans.pdf

THESIS_SOURCE = $(THESIS_DIR)/My_thesis.tex

THESIS_PARTS =					\
$(THESIS_DIR)/my_folder/title.tex		\
$(THESIS_DIR)/my_folder/introduction.tex	\
$(THESIS_DIR)/my_folder/chapter1.tex		\
$(THESIS_DIR)/my_folder/chapter2.tex		\
$(THESIS_DIR)/my_folder/chapter3.tex		\
$(THESIS_DIR)/my_folder/chapter4.tex		\
$(THESIS_DIR)/my_folder/chapter5.tex		\
$(THESIS_DIR)/my_folder/conclusion.tex		\
$(THESIS_DIR)/my_folder/my_biblio.bib


THESIS = $(THESIS_DIR)/My_thesis.pdf

.DEFAULT_GOAL = $(THESIS)

$(BASIC_DATASET) : generate_basic.R
	mkdir --parent $(DATASET_DIR)
	Rscript generate_basic.R

$(BASIC_TRUE_FIGURE) : $(BASIC_DATASET) plot_true_basic.R
	mkdir --parent  $(FIGURES_DIR)
	Rscript plot_true_basic.R
	pdfcrop --margins "10 10 5 5" $(BASIC_TRUE_FIGURE) $(BASIC_TRUE_FIGURE).cropped
	mv --force $(BASIC_TRUE_FIGURE).cropped $(BASIC_TRUE_FIGURE)

$(BASIC_SVC_TABLE) : $(BASIC_DATASET) svc.R adjust_svc_basic.R
	mkdir --parent  $(TABLES_DIR)
	Rscript adjust_svc_basic.R

$(BASIC_SVC_CLUSTERED) : $(BASIC_DATASET) svc.R cluster_svc_basic.R
	mkdir --parent  $(CLUSTERED_DIR)
	Rscript cluster_svc_basic.R

$(BASIC_SVC_FIGURE) : $(BASIC_SVC_CLUSTERED) plot_svc_basic.R
	mkdir --parent  $(FIGURES_DIR)
	Rscript plot_svc_basic.R
	pdfcrop --margins "10 10 5 5" $(BASIC_SVC_FIGURE) $(BASIC_SVC_FIGURE).cropped
	mv --force $(BASIC_SVC_FIGURE).cropped $(BASIC_SVC_FIGURE)

$(OVERLAPPING_DATASET) : generate_overlapping.R
	mkdir --parent $(DATASET_DIR)
	Rscript generate_overlapping.R

$(OVERLAPPING_TRUE_FIGURE) : $(OVERLAPPING_DATASET) plot_true_overlapping.R
	mkdir --parent $(FIGURES_DIR)
	Rscript plot_true_overlapping.R
	pdfcrop --margins "10 10 5 5" $(OVERLAPPING_TRUE_FIGURE) $(OVERLAPPING_TRUE_FIGURE).cropped
	mv --force $(OVERLAPPING_TRUE_FIGURE).cropped $(OVERLAPPING_TRUE_FIGURE)

$(OVERLAPPING_KMEANS_TABLE) : $(OVERLAPPING_DATASET) adjust_kmeans_overlapping.R
	mkdir --parent  $(TABLES_DIR)
	Rscript adjust_kmeans_overlapping.R

$(OVERLAPPING_SVC_TABLE) : $(OVERLAPPING_DATASET) adjust_svc_overlapping.R
	mkdir --parent $(TABLES_DIR)
	Rscript adjust_svc_overlapping.R

$(OVERLAPPING_SVC_CLUSTERED) : $(BASIC_DATASET) svc.R cluster_svc_overlapping.R
	mkdir --parent $(CLUSTERED_DIR)
	Rscript cluster_svc_overlapping.R

$(OVERLAPPING_KMEANS_CLUSTERED) : $(BASIC_DATASET) cluster_kmeans_overlapping.R
	mkdir --parent $(CLUSTERED_DIR)
	Rscript cluster_kmeans_overlapping.R

$(OVERLAPPING_SVC_FIGURE) : $(OVERLAPPING_SVC_CLUSTERED) plot_svc_overlapping.R
	mkdir --parent $(FIGURES_DIR)
	Rscript plot_svc_overlapping.R
	pdfcrop --margins "10 10 5 5" $(OVERLAPPING_SVC_FIGURE) $(OVERLAPPING_SVC_FIGURE).cropped
	mv --force $(OVERLAPPING_SVC_FIGURE).cropped $(OVERLAPPING_SVC_FIGURE)

$(OVERLAPPING_KMEANS_FIGURE) : $(OVERLAPPING_KMEANS_CLUSTERED) plot_kmeans_overlapping.R
	mkdir --parent $(FIGURES_DIR)
	Rscript plot_kmeans_overlapping.R
	pdfcrop --margins "10 10 5 5" $(OVERLAPPING_KMEANS_FIGURE) $(OVERLAPPING_KMEANS_FIGURE).cropped
	mv --force $(OVERLAPPING_KMEANS_FIGURE).cropped $(OVERLAPPING_KMEANS_FIGURE)

$(CONCENTRIC_DATASET) : generate_concentric.R
	mkdir --parent $(DATASET_DIR)
	Rscript generate_concentric.R

$(CONCENTRIC_SVC_CLUSTERED) : $(BASIC_DATASET) svc.R cluster_svc_concentric.R
	mkdir --parent $(CLUSTERED_DIR)
	Rscript cluster_svc_concentric.R

$(CONCENTRIC_KMEANS_CLUSTERED) : $(BASIC_DATASET) cluster_kmeans_concentric.R
	mkdir --parent $(CLUSTERED_DIR)
	Rscript cluster_kmeans_concentric.R

$(CONCENTRIC_SVC_FIGURE) : $(CONCENTRIC_SVC_CLUSTERED) plot_svc_concentric.R
	mkdir --parent $(FIGURES_DIR)
	Rscript plot_svc_concentric.R
	pdfcrop --margins "10 10 5 5" $(CONCENTRIC_SVC_FIGURE) $(CONCENTRIC_SVC_FIGURE).cropped
	mv --force $(CONCENTRIC_SVC_FIGURE).cropped $(CONCENTRIC_SVC_FIGURE)

$(CONCENTRIC_KMEANS_FIGURE) : $(CONCENTRIC_KMEANS_CLUSTERED) plot_kmeans_concentric.R
	mkdir --parent $(FIGURES_DIR)
	Rscript plot_kmeans_concentric.R
	pdfcrop --margins "10 10 5 5" $(CONCENTRIC_KMEANS_FIGURE) $(CONCENTRIC_KMEANS_FIGURE).cropped
	mv --force $(CONCENTRIC_KMEANS_FIGURE).cropped $(CONCENTRIC_KMEANS_FIGURE)

$(IRIS_SVC_CLUSTERED) : $(BASIC_DATASET) svc.R cluster_svc_iris.R
	mkdir --parent $(CLUSTERED_DIR)
	Rscript cluster_svc_iris.R

$(IRIS_SVC_FIGURE) : $(IRIS_SVC_CLUSTERED) plot_svc_iris.R
	mkdir --parent $(FIGURES_DIR)
	Rscript plot_svc_iris.R
	pdfcrop --margins "10 10 5 5" $(IRIS_SVC_FIGURE) $(IRIS_SVC_FIGURE).cropped
	mv --force $(IRIS_SVC_FIGURE).cropped $(IRIS_SVC_FIGURE)

$(IRIS_TRUE_FIGURE) : $(IRIS_TRUE_CLUSTERED) plot_true_iris.R
	mkdir --parent $(FIGURES_DIR)
	Rscript plot_true_iris.R
	pdfcrop --margins "10 10 5 5" $(IRIS_TRUE_FIGURE) $(IRIS_TRUE_FIGURE).cropped
	mv --force $(IRIS_TRUE_FIGURE).cropped $(IRIS_TRUE_FIGURE)

$(IRIS_KMEANS_CLUSTERED) : $(BASIC_DATASET) cluster_kmeans_iris.R
	mkdir --parent $(CLUSTERED_DIR)
	Rscript cluster_kmeans_iris.R

$(IRIS_KMEANS_FIGURE) : $(IRIS_KMEANS_CLUSTERED) plot_kmeans_iris.R
	mkdir --parent $(FIGURES_DIR)
	Rscript plot_kmeans_iris.R
	pdfcrop --margins "10 10 5 5" $(IRIS_KMEANS_FIGURE) $(IRIS_KMEANS_FIGURE).cropped
	mv --force $(IRIS_KMEANS_FIGURE).cropped $(IRIS_KMEANS_FIGURE)

$(THESIS) : $(BASIC_SVC_TABLE) $(BASIC_SVC_FIGURE)			\
		$(OVERLAPPING_TRUE_FIGURE) $(OVERLAPPING_KMEANS_TABLE)	\
		$(OVERLAPPING_SVC_TABLE) $(OVERLAPPING_SVC_FIGURE)	\
		$(OVERLAPPING_KMEANS_FIGURE) $(IRIS_SVC_FIGURE)		\
		$(IRIS_TRUE_FIGURE) $(IRIS_KMEANS_FIGURE) $(THESIS_SOURCE) $(THESIS_PARTS)	\
		$(CONCENTRIC_SVC_FIGURE) $(CONCENTRIC_KMEANS_FIGURE) clear_thesis
	cd $(THESIS_DIR); \
	pdflatex -halt-on-error My_thesis.tex; \
	pdflatex -halt-on-error My_thesis.tex; \
	biber My_thesis; \
	pdflatex -halt-on-error My_thesis.tex; \
	pdflatex -halt-on-error My_thesis.tex;

clear_thesis :
	cd $(THESIS_DIR); \
	rm -f My_thesis.{aux,bbl,bcf,blg,log,out,run.xml,toc,pdf,synctex.gz} *.idx
