X <- readRDS("data/overlapping_data.rds")
true.labels <- readRDS("data/overlapping_labels.rds")
kmeans.labels <- readRDS("clustered/overlapping_kmeans.rds")

labels <- integer(length(kmeans.labels))
labels[kmeans.labels == 3] <- 1
labels[kmeans.labels == 1] <- 2
labels[kmeans.labels == 2] <- 3

colors <- character(length(labels))
colors[labels == 1] <- "red"
colors[labels == 2] <- "skyblue"
colors[labels == 3] <- "green"
colors[labels != true.labels] <- "black"

cairo_pdf(filename = "figures/overlapping_kmeans.pdf", width = 5, height = 5)
plot(X, type = "p", col = colors, xlab = "", ylab = "", pch = 20, cex = 0.8)
dev.off()
