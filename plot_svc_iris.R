library(stats)
library(datasets)
data("iris")

X <- prcomp(iris[,1:4])$x[,1:2]

true.labels <- integer(nrow(iris))
true.labels[iris[,5]=="setosa"] <- 1
true.labels[iris[,5]=="versicolor"] <- 2
true.labels[iris[,5]=="virginica"] <- 3

result <- readRDS("clustered/iris_svc.rds")

colors <- character(length(result$labels))
colors[result$labels == 1] <- "red"
colors[result$labels == 2] <- "skyblue"
colors[result$labels == 3] <- "green"

cairo_pdf(filename = "figures/iris_svc.pdf", width = 5, height = 5)
plot(X, type = "n", xlab = "", ylab = "") # Hack to create axes with optimal limits
points(X[result$types != 2 & result$labels == true.labels,],
       col = colors[result$types != 2 & result$labels == true.labels], pch = 20, cex = 0.8)
points(X[result$types == 2 & result$labels == true.labels,],
       col = colors[result$types == 2 & result$labels == true.labels], pch = 4, cex = 0.4)
points(X[result$types == 1,], col = "black", pch = 1, lwd = 0.5, cex = 0.6)
points(X[result$labels != true.labels,], col = "black", pch = 20, cex = 0.7)
dev.off()
